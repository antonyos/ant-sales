import Api from "src/services/Api";

export default {
  register(credentials) {
    return Api().post("register", credentials);
  },
};
