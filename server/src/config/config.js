module.exports = {
  port: process.env.PORT || 8081,
  db: {
    database: process.env.DB_NAME || 'ant_sales',
    user: process.env.DB_USER || 'ant_sales',
    password: process.env.DB_PASS || 'ant_sales',
    options: {
      dialect: process.env.DIALECT || 'sqlite',
      host: process.env.HOST || 'localhost',
      storage: './ant_sales.sqlite',
    },
  },
};
